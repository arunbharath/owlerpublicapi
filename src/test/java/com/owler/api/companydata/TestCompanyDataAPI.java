/**
 * 
 */
package com.owler.api.companydata;

import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.owler.api.util.SignatureUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * @author Jay
 *
 */
public class TestCompanyDataAPI {
	
	ClientConfig config;
    Client client;
    String secret="";
    
    int EXP_SUCCESS_STATUS = 200;
    int EXP_FAIL_STATUS_400 = 400;
    int EXP_FAIL_STATUS_404 = 404;
    
  @BeforeClass
  public void beforeClass() throws JsonGenerationException, JsonMappingException, IOException {
      config = new DefaultClientConfig();
      client = Client.create(config);
}

  @Test
  @Parameters({"companyId"})
  public void testCompanyDataAPI(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS));
  }
  
  @Test
  @Parameters({"companyId"})
  public void testCompanyDataAPI_withNULLWebsite(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_withNULLWebsite <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_404));
  }
  
  @Test
  @Parameters({"companyId"})
  public void testCompanyDataAPI_withInValidID(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_withInValidID <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400));
  }
  
  @Test
  @Parameters({"companyId"})
  public void testCompanyDataAPI_withNonExistingID(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_withNonExistingID <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_404));
  }
  
  @Test
  @Parameters({"companyId"})
  public void testCompanyDataAPI_withRejectedId(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_withRejectedId <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_404));
  }
  
  @Test
  @Parameters({"companyId"})
  public void testCompanyDataAPI_withBlankId(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_withBlankId <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400));
  }
  
  @Test
  @Parameters({"companyId"})
  public void testCompanyDataAPI_verifyXMLResponseStructure(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_verifyXMLResponseStructure <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI_verifyXMLResponseStructure(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  System.out.println(">>>>>>>>>> :"+response.getEntity(String.class));
	  assertTrue(  (response.getStatus()==EXP_SUCCESS_STATUS)); //.getEntity(String.class).toString().contains("xml version")));
  }
  
  
  @Test
  @Parameters({"companyUrl"})
  public void testCompanyDataAPI_URL(String companyUrl) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_URL <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI_URL(client, companyUrl);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS));
  }
  
  
  @Test
  @Parameters({"companyUrl"})
  public void testCompanyDataAPI_URL_Invalid(String companyUrl) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_URL_Invalid <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI_URL(client, companyUrl);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_404));
  }
  
  
  @Test
  @Parameters({"companyUrl"})
  public void testCompanyDataAPI_URL_Blank(String companyUrl) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_URL_Blank <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI_URL(client, companyUrl);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400));
  }
  
  @Test
  @Parameters({"companyUrl"})
  public void testCompanyDataAPI_URL_verifyXMLResponseStructure(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompanyDataAPI_URL_verifyXMLResponseStructure <<<<");
	  ClientResponse response = CompanyDataAPI.invokeCompanyDataAPI_URL_verifyXMLResponseStructure(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  System.out.println(">>>>>>>>>> :"+response.getEntity(String.class));
	  assertTrue(  (response.getStatus()==EXP_SUCCESS_STATUS)); //getEntity(String.class).toString().contains("xml version")));
  }
  
  
  @AfterClass
  public void afterClass() {
  }
}
