package com.owler.api.datacard;

import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.NavigableMap;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrDocumentList;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Ignore;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.owler.api.companydata.CompanyDataAPI;
import com.owler.api.search.SearchAPI;
import com.owler.api.util.SignatureUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.owler.library.MongoDB;
//import com.owler.library.SolrLibrary;

public class TestDatacardAPI {
	
	ClientConfig config;
    Client client;
    String secret="";
    
    int EXP_SUCCESS_STATUS = 200;
    int EXP_FAIL_STATUS_400 = 400;
    int EXP_FAIL_STATUS_404 = 404;
    int EXP_FAIL_STATUS_500 = 500;
    
    MongoDB ConnectMongoDB = new MongoDB();
//    SolrLibrary ConnectSolr = new SolrLibrary();   
    SolrDocumentList SolrResponse = new SolrDocumentList();
    
    @BeforeClass
    public void beforeClass() throws JsonGenerationException, JsonMappingException, IOException {
        config = new DefaultClientConfig();
        client = Client.create(config);
    }
    
    @BeforeTest
    public void Setup() throws ExceptionInInitializerError, SolrServerException, MalformedURLException, JSONException{
 
        ConnectMongoDB.ConnectToMongoDatabase("owler_contribution");
//        SolrResponse = ConnectSolr.GetSolrResponse("member_relevancy_score","*:*");
    }
    
    @Test
    public void EmployeeDCEndPointtoEventsPage() throws IOException, Exception{
    	String[] userVotes = {"EMP_LB", "EMP_IN_RANGE", "EMP_UB", "EMP_LT_10", "EMP_GT_10","DONT_KNOW"};
    	System.out.println(">>>> EmployeeDC StoreVote Test <<<<");
    	DatacardQuestion dcResponse = ConnectMongoDB.getDatacardQuestion("EMPLOYEE_HEAD_COUNT", 1);
    	for(String vote : userVotes){
	    	ClientResponse response = DatacardAPI.EMPLOYEE_DCEndPointToEventsPageStoreVote(client,dcResponse,vote);
	      	Boolean status = DatacardResponseVerification.EMPLOYEE_StoreVoteVerrification(dcResponse,response,vote);
	    	System.out.println("=====================================================================================");
	    	assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) &&  status==true);
    	}
    }
    
    @Test
    public void RevenueDCEndPointtoEventsPage() throws IOException, Exception{
    	String[] userVotes = {"REV_LB","REV_IN_RANGE","REV_UB","REV_LT_1M","REV_GT_1M","DONT_KNOW"}; //"REV_EXACT","REV_EXACT_NEW",
    	System.out.println(">>>> RevenueDC StoreVote Test <<<<");
    	DatacardQuestion dcResponse = ConnectMongoDB.getDatacardQuestion("REVENUE", 1);
    	for(String vote : userVotes){
	    	ClientResponse response = DatacardAPI.REVENUE_DCEndPointToEventsPageStoreVote(client,dcResponse,vote);
	      	Boolean status = DatacardResponseVerification.REVENUE_StoreVoteVerrification(dcResponse,response,vote);
	    	System.out.println("=====================================================================================");
	    	assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) &&  status==true);
    	}
    }
    
    @Test
    public void CompetitorDCEndPointtoEventsPage() throws IOException, Exception{
    	String[] userVotes = {"COMPETITOR_YES","COMPETITOR_NO","DONT_KNOW"}; //
    	System.out.println(">>>> CompetitorDC StoreVote Test <<<<");
    	DatacardQuestion dcResponse = ConnectMongoDB.getDatacardQuestion("COMPETITOR", 1);
    	for(String vote : userVotes){
	    	ClientResponse response = DatacardAPI.COMPETITOR_DCEndPointToEventsPageStoreVote(client,dcResponse,vote);
	      	Boolean status = DatacardResponseVerification.COMPETITOR_StoreVoteVerrification(dcResponse,response,vote);
	    	System.out.println("=====================================================================================");
	    	assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) &&  status==true);
    	}
    }
    
    @Test
    public void SectorDCEndPointtoEventsPage() throws IOException, Exception{
    	String[] userVotes = {"SECTOR_YES","SECTOR_NO"};
    	System.out.println(">>>> SectorDC StoreVote Test <<<<");
    	DatacardQuestion dcResponse = ConnectMongoDB.getDatacardQuestion("SECTOR", 1);
    	for(String vote : userVotes){
	    	ClientResponse response = DatacardAPI.SECTOR_DCEndPointToEventsPageStoreVote(client,dcResponse,vote);
	      	Boolean status = DatacardResponseVerification.SECTOR_StoreVoteVerrification(dcResponse,response,vote);
	    	System.out.println("=====================================================================================");
	    	assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) &&  status==true);
    	}
    }

    @Test
    public void SD_DCEndPointtoEventsPage() throws IOException, Exception{
    	String[] userVotes = {"GO_UP","GO_DOWN","STAY_FLAT"};
    	System.out.println(">>>> Stock-DirectionDC StoreVote Test <<<<");
    	DatacardQuestion dcResponse = ConnectMongoDB.getDatacardQuestion("STOCK_DIRECTION", 1);
    	for(String vote : userVotes){
    		ClientResponse response = DatacardAPI.SD_DCEndPointToEventsPageStoreVote(client,dcResponse,vote);
		  	Boolean status = DatacardResponseVerification.SD_StoreVoteVerrification(dcResponse,response,vote);
			System.out.println("=====================================================================================");
			assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) &&  status==true);
    	}
    }
    
    @Test
    public void LO_DCEndPointtoEventsPage() throws IOException, Exception{
    	String[] userVotes = {"IPO", "FAIL", "GET_BOUGHT"};
    	System.out.println(">>>> Likely-OutcomeDC StoreVote Test <<<<");
    	DatacardQuestion dcResponse = ConnectMongoDB.getDatacardQuestion("COMPANY_LIKELY_OUTCOME", 1);
    	for(String vote : userVotes){
	    	ClientResponse response = DatacardAPI.LO_DCEndPointToEventsPageStoreVote(client,dcResponse,vote);
	      	Boolean status = DatacardResponseVerification.LO_StoreVoteVerrification(dcResponse,response,vote);
	    	System.out.println("=====================================================================================");
	    	assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) &&  status==true);
    	}
    }
    
    @Test
    public void CA_DCEndPointtoEventsPage() throws IOException, Exception{
    	String[] userVotes = {"GOOD","BAD","NEUTRAL"};
    	System.out.println(">>>> CEO-ApprovalDC StoreVote Test <<<<");
    	DatacardQuestion dcResponse = ConnectMongoDB.getDatacardQuestion("CEO_APPROVAL", 1);
    	for(String vote : userVotes){
	    	ClientResponse response = DatacardAPI.CA_DCEndPointToEventsPageStoreVote(client,dcResponse,vote);
	      	Boolean status = DatacardResponseVerification.CA_StoreVoteVerrification(dcResponse,response,vote);
	    	System.out.println("=====================================================================================");
	    	assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) &&  status==true);
    	}
    }
    
    @Test
    public void DatacardsToEventsPage() throws IOException,Exception{
    	long memberId=1465065;
    	long companyIds[] = {3515623, 137609, 4396832, 240113, 209227, 100206, 125271, 7302833, 100217, 4493371, 7263835, 221415, 105115, 100163, 138727, 2047093, 100168, 701145, 1819187, 127168, 365160, 149519, 101363, 7296301, 222453, 194281, 102324, 121413, 115690, 10766994, 8357960, 4810968, 9486162, 122392, 10023816, 4406661, 133344, 101359, 357761, 154924, 1794335, 154923, 124398, 1867961, 1531486, 127121, 103027, 154918, 103018, 1594383, 3533413, 163666, 154928, 1190817, 12025943, 4809939, 146281, 106061, 7570572, 12042441, 135562, 266730, 1169222, 5487964, 126340, 103053, 1278622, 4809933, 4377072, 144358, 10869760, 126339, 137503, 4440930, 12025980, 1171427, 102211, 378646, 364290, 5637595, 8696356, 124208, 5209383, 12064473, 171112, 113766, 124229, 1794483, 133232, 255334, 164349, 10212195, 263379, 135625, 134177, 139922, 706941, 122538, 243745, 9338778, 8036464, 2665933, 138554, 1853742, 3722806, 247609, 100242, 163801, 234188, 12026313, 193521, 5887491, 129405, 129404, 3899269, 2232845, 102139, 228382, 123078, 101043, 1413722, 253792, 104282, 102130, 157577, 190100, 5002359, 295960, 137348, 137349, 135197, 12123190, 135211, 244218, 7398863, 225512, 220762, 12013907, 261418, 4493681, 135234, 1634303, 137422, 113057, 137421, 142235, 12051315, 106499, 4392796, 132384, 250875, 135257, 225468, 5218064, 183324, 4391719, 9473318, 135284, 1627147, 715438, 587626, 4379928, 128292, 151009, 135305, 128291, 9715654, 1865668, 6937882, 141188, 207200, 4430689, 291358, 12010625, 3522131, 184636, 12056319, 12042722, 129501, 251723, 7291320, 2440794, 12052434, 221528, 1188583, 196591, 127274, 1486612, 135368, 260497, 9512029, 101935, 140178, 276110, 100978, 12031077, 100979, 100980, 9406161, 120612, 6798133, 135403, 2238753, 122801, 484608, 6002255, 301315, 1476881, 122795, 1537320, 197631, 122796, 1865659, 233010, 1175038, 5046425, 2693103, 4198073, 4442481, 139245, 101887, 138126, 8765808, 139206, 150075, 125774, 124828, 125768, 1724395, 131634, 213126, 11929864, 125755, 123811, 1793176, 4874220, 12017989, 272586, 227324, 196718, 139325, 128765, 208608, 128766, 365651, 4442391, 184104, 3134009, 132640, 12027613, 118053, 125706, 1420943, 217596, 127654, 127656, 361849, 169497, 3255349, 1174262, 1401654, 9711661, 128539, 2266338, 1838078, 1855292, 222728, 126888, 11559876, 222734, 139507, 164747, 4408338, 124677, 124679, 124678, 4379204, 1472673, 139072, 124680, 445416, 100651, 12024914, 4421456, 12024915, 658087, 995067, 4428012, 100654, 496807, 1386441, 189705, 141362, 125883, 125886, 12010414, 2516536};
    	System.out.println(">>>> Events Page DC End Point <<<<");
    	for(long companyId : companyIds){
    		ClientResponse response = DatacardAPI.Get_DCEndPointToEventsPage(client,memberId,companyId);
    		Boolean status = DatacardResponseVerification.Get_DCEndPointToEventsPage(response);
    		System.out.println("=====================================================================================");
	    	assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) &&  status==true);
    	}
    }
    @Test
    public void AppropriatenessScoreImprovement() throws IOException,Exception
    {
    	long memberId=1465065;
    	long companyIds[] = {3515623, 137609, 4396832, 240113, 209227, 100206, 125271, 7302833, 100217, 4493371, 7263835, 221415, 105115, 100163, 138727, 2047093, 100168, 701145, 1819187, 127168, 365160, 149519, 101363, 7296301, 222453, 194281, 102324, 121413, 115690, 10766994, 8357960, 4810968, 9486162, 122392, 10023816, 4406661, 133344, 101359, 357761, 154924, 1794335, 154923, 124398, 1867961, 1531486, 127121, 103027, 154918, 103018, 1594383, 3533413, 163666, 154928, 1190817, 12025943, 4809939, 146281, 106061, 7570572, 12042441, 135562, 266730, 1169222, 5487964, 126340, 103053, 1278622, 4809933, 4377072, 144358, 10869760, 126339, 137503, 4440930, 12025980, 1171427, 102211, 378646, 364290, 5637595, 8696356, 124208, 5209383, 12064473, 171112, 113766, 124229, 1794483, 133232, 255334, 164349, 10212195, 263379, 135625, 134177, 139922, 706941, 122538, 243745, 9338778, 8036464, 2665933, 138554, 1853742, 3722806, 247609, 100242, 163801, 234188, 12026313, 193521, 5887491, 129405, 129404, 3899269, 2232845, 102139, 228382, 123078, 101043, 1413722, 253792, 104282, 102130, 157577, 190100, 5002359, 295960, 137348, 137349, 135197, 12123190, 135211, 244218, 7398863, 225512, 220762, 12013907, 261418, 4493681, 135234, 1634303, 137422, 113057, 137421, 142235, 12051315, 106499, 4392796, 132384, 250875, 135257, 225468, 5218064, 183324, 4391719, 9473318, 135284, 1627147, 715438, 587626, 4379928, 128292, 151009, 135305, 128291, 9715654, 1865668, 6937882, 141188, 207200, 4430689, 291358, 12010625, 3522131, 184636, 12056319, 12042722, 129501, 251723, 7291320, 2440794, 12052434, 221528, 1188583, 196591, 127274, 1486612, 135368, 260497, 9512029, 101935, 140178, 276110, 100978, 12031077, 100979, 100980, 9406161, 120612, 6798133, 135403, 2238753, 122801, 484608, 6002255, 301315, 1476881, 122795, 1537320, 197631, 122796, 1865659, 233010, 1175038, 5046425, 2693103, 4198073, 4442481, 139245, 101887, 138126, 8765808, 139206, 150075, 125774, 124828, 125768, 1724395, 131634, 213126, 11929864, 125755, 123811, 1793176, 4874220, 12017989, 272586, 227324, 196718, 139325, 128765, 208608, 128766, 365651, 4442391, 184104, 3134009, 132640, 12027613, 118053, 125706, 1420943, 217596, 127654, 127656, 361849, 169497, 3255349, 1174262, 1401654, 9711661, 128539, 2266338, 1838078, 1855292, 222728, 126888, 11559876, 222734, 139507, 164747, 4408338, 124677, 124679, 124678, 4379204, 1472673, 139072, 124680, 445416, 100651, 12024914, 4421456, 12024915, 658087, 995067, 4428012, 100654, 496807, 1386441, 189705, 141362, 125883, 125886, 12010414, 2516536};
    	System.out.println(">>>> Appropriateness Score Improvement <<<<");
    	for(long companyId : companyIds)
    	{
    		ClientResponse response = DatacardAPI.Get_DCToSnapshot(client,memberId,companyId);
    		String  responseToEventsPage = response.getEntity(String.class);
    		JSONArray jsonArray = new JSONArray(responseToEventsPage.trim());
    		String responseInString = jsonArray.getString(0);
    		JSONObject jsonObject = new JSONObject(responseInString.trim());
    		DatacardHistory dcHistoryResponse = ConnectMongoDB.getDCHistory(jsonObject.getString("id"), memberId);
    		Boolean status = DatacardResponseVerification.Get_DCToSnapshot(response,dcHistoryResponse);
    		System.out.println("=====================================================================================");
    		assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) &&  status==true);
    	}
    	
    }
}
