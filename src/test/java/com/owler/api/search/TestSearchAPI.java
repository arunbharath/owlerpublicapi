/**
 * 
 */
package com.owler.api.search;

import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.owler.api.companydata.CompanyDataAPI;
import com.owler.api.search.SearchAPI;
import com.owler.api.util.SignatureUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * @author Jay
 *
 */
public class TestSearchAPI {
	
	ClientConfig config;
    Client client;
    String secret="";
    
    int EXP_SUCCESS_STATUS = 200;
    int EXP_FAIL_STATUS_400 = 400;
    int EXP_FAIL_STATUS_404 = 404;
    
    @BeforeClass
    public void beforeClass() throws JsonGenerationException, JsonMappingException, IOException {
        config = new DefaultClientConfig();
        client = Client.create(config);
  }

    @Test
    @Parameters({"searchStr"})
    public void testSearchAPI_Name(String searchStr) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testSearchAPI_Name <<<<");
  	  ClientResponse response = SearchAPI.invokeSearchAPI_Name(client, searchStr);
    	
  	  System.out.println("=====================================================================================");
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS));
    }
    
    @Test
    @Parameters({"searchStr"})
    public void testSearchAPI_Name_HttpStatusCode400(String searchStr) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> SearchAPI_name_Morethan128Chars_HttpStatusCode400 <<<<");
  	  ClientResponse response = SearchAPI.invokeSearchAPI_Name(client, searchStr);
    	
  	  System.out.println("=====================================================================================");
  	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400));
    }
    
    @Test
    @Parameters({"searchStr"})
    public void testSearchAPI_InvalidFieldName_HttpStatusCode400(String searchStr) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testSearchAPI_InvalidFieldName_HttpStatusCode400 <<<<");
  	  ClientResponse response = SearchAPI.invokeSearchAPI_InvalidFieldName(client, searchStr);
    	
  	  System.out.println("=====================================================================================");
  	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400));
    }
    
    @Test
    @Parameters({"searchStr"})
    public void testSearchAPI_MaxLimitVal30(String searchStr) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testSearchAPI_MaxLimitVal30 <<<<");
  	  ClientResponse response = SearchAPI.invokeSearchAPI_MaxLimitVal30(client, searchStr);
    	
  	  System.out.println("=====================================================================================");
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS));
    }
    
    @Test
    @Parameters({"searchStr"})
    public void testSearchAPI_InvalidQueryStr_HttpStatusCode400(String searchStr) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testSearchAPI_InvalidQueryStr_HttpStatusCode400 <<<<");
  	  ClientResponse response = SearchAPI.invokeSearchAPI_InvalidQueryStr(client, searchStr);
    	
  	  System.out.println("=====================================================================================");
  	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400));
    }
    
    @Test
    @Parameters({"searchStr"})
    public void testSearchAPI_Website(String searchStr) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testSearchAPI_Website <<<<");
  	  ClientResponse response = SearchAPI.invokeSearchAPI_Website(client, searchStr);
    	
  	  System.out.println("=====================================================================================");
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS));
    }
   

    @Test
    @Parameters({"searchStr"})
    public void testSearchAPI_Ticker(String searchStr) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testSearchAPI_Ticker <<<<");
  	  ClientResponse response = SearchAPI.invokeSearchAPI_Ticker(client, searchStr);
    	
  	  System.out.println("=====================================================================================");
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS));
    }
    
    
    @AfterClass
    public void afterClass() {
    }
  }