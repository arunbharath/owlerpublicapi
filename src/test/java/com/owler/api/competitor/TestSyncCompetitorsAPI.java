/**
 * 
 */
package com.owler.api.competitor;

import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.json.JSONConfiguration;

/**
 * @author Jay
 * 
 */
public class TestSyncCompetitorsAPI {
	static ClientConfig config;
	static Client client;
	static String secret = "";

	int EXP_SUCCESS_STATUS = 200;
	int EXP_FAIL_STATUS_400 = 400;
	int EXP_FAIL_STATUS_404 = 404;

	@BeforeClass
	public static void beforeClass() throws JsonGenerationException,
			JsonMappingException, IOException {
		config = new DefaultClientConfig();
		config.getProperties()
				.put(ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
				Boolean.TRUE);
		config.getClasses().add(JacksonJsonProvider.class);
		client = Client.create(config);
		String secretKey = "LaGGm0DAd2zDs";
		String memberId = "999999";

		client.addFilter(new RestClientAuthFilter(secretKey, memberId));
	}

	@Test
	public void testSyncCompetitorsAPI() throws IOException, Exception {

		System.out.println(">>>> testSyncCompetitorsAPI <<<<");
		ClientResponse response = SyncCompetitorsAPI
				.invokeSyncCompetitorIdAPI(client);

		System.out
				.println("=====================================================================================");
		assertTrue((response.getStatus() == EXP_SUCCESS_STATUS));
		// TODO:
		// Verify the status code (done already)
		// Verify the member_company_competitors collection with the mentioned
		// member id: 9876543. like
		// db.member_company_competitors.find({"member_id":9876543})
		// Verify there are 2 docs returned for the above query
		// Verify below JSON structure
		/*
		 * { "_id" : ObjectId("56fe40d5877aaa63e2bd457b"), "company_id" :
		 * NumberLong(100001), "member_id" : NumberLong(9876543),
		 * "competitors_list" : [ NumberLong(100002), NumberLong(100006) ],
		 * "update_ts" : ISODate("2016-04-01T09:29:12.777Z") }
		 * 
		 * 
		 * { "_id" : ObjectId("56fe40d5877aaa63e2bd457a"), "company_id" :
		 * NumberLong(100002), "member_id" : NumberLong(9876543),
		 * "competitors_list" : [ NumberLong(100009), NumberLong(100006) ],
		 * "update_ts" : ISODate("2016-04-01T09:29:12.763Z") }
		 */
	}

}

class RestClientAuthFilter extends ClientFilter {

	String secretKey, memberId;
	public static final String X_SIGNATURE_HEADER_KEY = "X-signature";
	public static final String REST_REQUEST_MEMBER_ID_HEADER = "X-member-id";

	public RestClientAuthFilter(String secretKey, String memberId) {
		super();
		this.secretKey = secretKey;
		this.memberId = memberId;
	}

	@Override
	public ClientResponse handle(ClientRequest request) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String entityString = mapper
					.writeValueAsString(request.getEntity());
			String computedSignature = HmacSignatureUtil.computeSignature(
					entityString.getBytes(), secretKey);
			request.getHeaders().add(X_SIGNATURE_HEADER_KEY, computedSignature);
			request.getHeaders().add(REST_REQUEST_MEMBER_ID_HEADER, memberId);
		} catch (IOException e) {
			throw new ClientHandlerException(e);
		} catch (InvalidKeyException ie) {
		} catch (NoSuchAlgorithmException nse) {
		}

		ClientResponse response = getNext().handle(request);
		return response;
	}
}

class HmacSignatureUtil {
	private static final String ALG_HMAC = "HmacSHA1";

	public static String computeSignature(byte[] contentToHash, String secretKey)
			throws NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(),
				ALG_HMAC);
		Mac mac = Mac.getInstance(ALG_HMAC);
		mac.init(signingKey);
		byte[] encBytes = mac.doFinal(contentToHash);
		String computedSignature = Base64.encodeBase64URLSafeString(encBytes)
				.trim();
		return computedSignature;
	}

}

/*
 * 
 * >>>> testSyncCompetitorsAPI <<<<
 * =================================================================== End Point
 * URL :http://52.1.75.146:8080/iaApp/iris/oscar/syncCompetitors/ Response
 * Status :200 Response Header :{Transfer-Encoding=[chunked], Date=[Fri, 01 Apr
 * 2016 09:31:22 GMT], X-Cacheable=[true], Content-Type=[text/html],
 * Server=[Apache-Coyote/1.1]} RESPONSE :{"100002":true,"100001":true}
 * ==========
 * ===========================================================================
 */