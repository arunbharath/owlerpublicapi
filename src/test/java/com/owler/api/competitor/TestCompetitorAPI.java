/**
 * 
 */
package com.owler.api.competitor;

import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.owler.api.util.SignatureUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * @author Jay
 *
 */
public class TestCompetitorAPI {
	
	ClientConfig config;
    Client client;
    String secret="";
    
    int EXP_SUCCESS_STATUS = 200;
    int EXP_FAIL_STATUS_400 = 400;
    int EXP_FAIL_STATUS_404 = 404;
    
  @BeforeClass
  public void beforeClass() throws JsonGenerationException, JsonMappingException, IOException {
      config = new DefaultClientConfig();
      client = Client.create(config);
}

  @Test
  @Parameters({"companyId"})
  public void testCompetitorIdAPI(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorIdAPI <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorIdAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS));
  }
  
  @Test
  @Parameters({"companyId"})
  public void testCompetitorIdAPI_BlankId(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorIdAPI_BlankId <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorIdAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400));
  }
  
  @Test
  @Parameters({"companyId"})
  public void testCompetitorInvalidIdAPI(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorInvalidIdAPI <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorIdAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_404));
  }
  
  
  @Test
  @Parameters({"companyId"})
  public void testCompetitorIdAPI_verifyXMLResponseStructure(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorIdAPI_verifyXMLResponseStructure <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorIdAPI_verifyXMLResponseStructure(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  System.out.println(">>>>>>>>>> :"+response.getEntity(String.class));
	  assertTrue(  (response.getStatus()==EXP_SUCCESS_STATUS)); //.getEntity(String.class).toString().contains("xml version")));
  }
  
  @Test
  @Parameters({"companyId"})
  public void testCompetitorIdAPI_whichHasNoCompetitors(String companyId) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorIdAPI_withNoCompetitors <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorIdAPI(client, companyId);
  	
	  System.out.println("=====================================================================================");
	  System.out.println(">>>>>>>>>> :"+response.getEntity(String.class));
	  assertTrue(  (response.getStatus()==EXP_SUCCESS_STATUS)); //.getEntity(String.class).toString().contains("xml version")));
  }
  
  
  
  
  
  
  
  @Test
  @Parameters({"companyURL"})
  public void testCompetitorUrlAPI(String companyUrl) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorUrlAPI <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorUrlAPI(client, companyUrl);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS));
  }
  
  @Test
  @Parameters({"companyURL"})
  public void testCompetitorUrlAPI_BlankUrl(String companyUrl) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorUrlAPI_BlankUrl <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorUrlAPI(client, companyUrl);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400));
  }
  
  @Test
  @Parameters({"companyURL"})
  public void testCompetitorInvalidUrlAPI(String companyUrl) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorInvalidUrlAPI <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorUrlAPI(client, companyUrl);
  	
	  System.out.println("=====================================================================================");
	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_404));
  }
  
  
  @Test
  @Parameters({"companyURL"})
  public void testCompetitorUrlAPI_verifyXMLResponseStructure(String companyUrl) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorUrlAPI_verifyXMLResponseStructure <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorUrlAPI_verifyXMLResponseStructure(client, companyUrl);
  	
	  System.out.println("=====================================================================================");
	  System.out.println(">>>>>>>>>> :"+response.getEntity(String.class));
	  assertTrue(  (response.getStatus()==EXP_SUCCESS_STATUS));
  }
  
  @Test
  @Parameters({"companyURL"})
  public void testCompetitorUrlAPI_whichHasNoCompetitors(String companyUrl) throws IOException, Exception{
	  	  
	  System.out.println(">>>> testCompetitorUrlAPI_whichHasNoCompetitors <<<<");
	  ClientResponse response = CompetitorAPI.invokeCompetitorUrlAPI(client, companyUrl);
  	
	  System.out.println("=====================================================================================");
	  System.out.println(">>>>>>>>>> :"+response.getEntity(String.class));
	  assertTrue(  (response.getStatus()==EXP_SUCCESS_STATUS));
  }
  
  @AfterClass
  public void afterClass() {
  }
}
