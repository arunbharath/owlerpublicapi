/**
 * 
 */
package com.owler.api.feed;

import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.owler.api.companydata.CompanyDataAPI;
import com.owler.api.search.SearchAPI;
import com.owler.api.util.SignatureUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * @author Jay
 *
 */
public class TestFeedAPI {
	
	ClientConfig config;
    Client client;
    String secret="";
    
    int EXP_SUCCESS_STATUS = 200;
    int EXP_FAIL_STATUS_400 = 400;
    int EXP_FAIL_STATUS_404 = 404;
    
    @BeforeClass
    public void beforeClass() throws JsonGenerationException, JsonMappingException, IOException {
        config = new DefaultClientConfig();
        client = Client.create(config);
  }

   @Test
   @Parameters({"companyId"})
    public void testFeedAPI_ValidHit(String companyId) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI with Valid hit<<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId(client, companyId);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		 Serializable respagination = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
				 respagination =  respMap.get("pagination_id");
				 System.out.println("respMapLst :"+respMapLst.toString());
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 		 
			 for(int i=0; i<respMapLst.size();i++){
				 if(respMapLst.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }
  	  
  	  System.out.println("Count :"+count);
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count == 10) && (!respagination.toString().equalsIgnoreCase(null)));
    }

    @Test
    @Parameters({"companyId"})  
    public void testFeedAPI_CompanyWithNoFeed(String companyId) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI Company With NoFeed<<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId(client, companyId);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		 Serializable respagination = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
				
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
		  	  
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && respMapLst.toString().equalsIgnoreCase("[]"));
    }  
 
    
    @Test
    @Parameters({"companyId"})
    public void testFeedAPI_CompanyWithNoCategoryFeed(String companyId, String Category) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_Company With No "+Category+"<<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_category(client, companyId,Category);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		 Serializable respagination = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
				
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
		  	  
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && respMapLst.toString().equalsIgnoreCase("[]"));
    } 
    
    @Test
    @Parameters({"companyId","category"}) 
    public void testFeedAPI_CompanyId_category(String companyId,String category) throws IOException, Exception{
    	
    	 boolean status;
  	  System.out.println(">>>> testFeedAPI_CompanyId_category pop<<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_category(client, companyId,category);
    	
  	  System.out.println("=====================================================================================");

  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		 HashMap<String, Serializable> respLst = null;
		 Serializable respCategoryLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
				 respLst =   (HashMap<String, Serializable>) respMapLst.get(0);
				 respCategoryLst =  respLst.get("category");
				 System.out.println("respMapLst :"+respCategoryLst.toString());
				
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }

			
			 for(int i=0; i<respMapLst.size();i++){
				 
				 String Category = (String) respMapLst.get(i).get("category");
				 
				 if (Category.equalsIgnoreCase(category)){
					 status = true;
				 }
				 
				 else {
					 status = false;
					 break;
					 }
				 
			 }

  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (status=true));
    }
    
    /*@Test
    @Parameters({"companyId"})
    public void testFeedAPI_CompanyId(String companyId) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId <<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId(client, companyId);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
			 for(int i=0; i<respMapLst.size();i++){
				 if(respMapLst.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  System.out.println("Count :"+count);
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count == 10));
    }
    
    
    @Test
    @Parameters({"companyId","limit"})
    public void testFeedAPI_CompanyId_limit(String companyId,String limit) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_Limit <<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_limit(client, companyId,limit);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
			 for(int i=0; i<respMapLst.size();i++){
				 if(respMapLst.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  System.out.println("Count :"+count);
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count ==100));
    }
    
    @Test
    @Parameters({"companyId","limit"})
    public void testFeedAPI_CompanyId_limit10(String companyId,String limit) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_Limit <<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_limit(client, companyId,limit);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
			 for(int i=0; i<respMapLst.size();i++){
				 if(respMapLst.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  System.out.println("Count :"+count);
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count ==respMapLst.size()));
    }
    
    @Test
    @Parameters({"companyId","category"})
    public void testFeedAPI_CompanyId_category(String companyId,String category) throws IOException, Exception{
    	// Nee to change the validation
  	  	  
    	int count=0;
    	
  	  System.out.println(">>>> testFeedAPI_CompanyId_category pop<<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_category(client, companyId,category);
    	
  	  System.out.println("=====================================================================================");

  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
			 for(int i=0; i<respMapLst.size();i++){
				 if(respMapLst.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  System.out.println("Count :"+count);
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count >= 1));
    }
   

    @Test
    @Parameters({"companyId","category"})
    public void testFeedAPI_CompanyId_category_NoResults(String companyId,String category) throws IOException, Exception{
  	  	  
    	int count=0;
    	
  	  System.out.println(">>>> testFeedAPI_CompanyId_category_NoResults <<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_category_NoResults(client, companyId,category);
    	
  	  System.out.println("=====================================================================================");

  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
				 
				 for(int i=0; i<respMapLst.size();i++){
					 if(respMapLst.get(i).containsKey("id")  ){ 
						 count++;
						 }
					 else { System.out.println("Not Found");}
				 }

			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count == 0)); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count == 0));
    }

    
    
    @Test
    @Parameters({"companyId","category"})
    public void testFeedAPI_CompanyId_InvalidCategory(String companyId,String category) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_InvalidCategory <<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_category(client, companyId,category);
    	
  	  System.out.println("=====================================================================================");
  	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400) );
  	  
    }
    
    @Test
    @Parameters({"companyId","limit"})
    public void testFeedAPI_CompanyId_limitLT10(String companyId,String limit) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_Limit Less than 10<<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_limitLT10(client, companyId,limit);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
			 for(int i=0; i<respMapLst.size();i++){
				 if(respMapLst.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  System.out.println("Count :"+count);
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count == 4));
    }
   
    
    @Test
    @Parameters({"companyId","category"})
    public void testFeedAPI_CompanyId_Category_resultsLT10(String companyId,String category) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_Limit Less than 10<<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_category(client, companyId,category);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
			 for(int i=0; i<respMapLst.size();i++){
				 if(respMapLst.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  System.out.println("Count :"+count);
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count == respMapLst.size()));
    }
    
    
    @Test
    @Parameters({"companyId","limit"})
    public void testFeedAPI_CompanyId_limitDefaultBlank(String companyId,String limit) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_Limit Default or Blank<<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_limitLT10(client, companyId,limit);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
			 for(int i=0; i<respMapLst.size();i++){
				 if(respMapLst.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  System.out.println("Count :"+count);
  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count == 10));
    }
    
    
    @Test
    @Parameters({"companyId","limit"})
    public void testFeedAPI_CompanyId_limitInvalid(String companyId,String limit) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_limitInvalid <<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_limit(client, companyId,limit);
    	
  	  System.out.println("=====================================================================================");
  	 
  	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400) );
    }
    
    
    @Test
    @Parameters({"companyId","limit"})
    public void testFeedAPI_CompanyId_limitGT100(String companyId,String limit) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_Limit Greater than 100<<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_limitGT100(client, companyId,limit);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst = null;
		  
			 try {
				 respMap = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap.toString());
				 respMapLst = (ArrayList<HashMap<String, Serializable>>) respMap.get("feeds");
				 System.out.println("respMapLst :"+respMapLst.toString());
			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400) && (count == 0)); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
//			 for(int i=0; i<10;i++){
//				 if(respMapLst.get(i).containsKey("id")  ){ 
//					 count++;
//					 }
//				 else { System.out.println("Not Found");}
//			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
//  	  System.out.println("Count :"+count);
  	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400) && (count == 0));
    }
   
    
    @Test
    @Parameters({"companyId"})
    public void testFeedAPI_CompanyId_PaginationId(String companyId) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_Pagination <<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId(client, companyId);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap1 = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst1 = null;
		 
		 String pgnId1 = null;
		 String fId1 = null;
		  
		 String pgnId2 = null;
		 String fId2 = null;
		 
			 try {
				 respMap1 = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap1.toString());
				 respMapLst1 = (ArrayList<HashMap<String, Serializable>>) respMap1.get("feeds");
				 System.out.println("respMapLst :"+respMapLst1.toString());
				 
			  		pgnId1 = (String) respMap1.get("pagination_id");
			  		fId1 = (String) respMapLst1.get(0).get("id");
			  		
			  		System.out.println("FIRST Id :"+fId1);
			  		System.out.println("FIRST pgn Id :"+pgnId1);

			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
			 for(int i=0; i<respMapLst1.size();i++){
				 if(respMapLst1.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  System.out.println("Count :"+count);

  	  if(count == 10) {
  		response = FeedAPI.invokeFeedAPI_CompanyId_PaginationId(client, companyId,pgnId1);

  		String pgnEntity = response.getEntity(String.class);
    	  System.out.println(" ENTITY :"+pgnEntity );

    	
  		 HashMap<String, Serializable> pgnMap = null;
  		 ArrayList<HashMap<String, Serializable>> pgnMapLst = null;
  		 
  			 try {
  				 pgnMap = new ObjectMapper().readValue(pgnEntity, HashMap.class);
  				 System.out.println("pgnMap :"+pgnMap.toString());
  				 pgnMapLst = (ArrayList<HashMap<String, Serializable>>) pgnMap.get("feeds");
  				 System.out.println("pgnMapLst :"+pgnMapLst.toString());
  				 
  			  		pgnId2 = (String) pgnMap.get("pagination_id");
  			  		fId2 = (String) pgnMapLst.get(0).get("id");
  			  		
  			  		System.out.println("SECOND Id :"+fId2);
  			  		System.out.println("SECOND pgnId :"+pgnId2);

  			 }
  			 catch (JsonParseException e) {	e.printStackTrace(); } 
  			 catch (JsonMappingException e) { e.printStackTrace(); } 
  			 catch (IOException e) { e.printStackTrace(); }
  		
  		assertTrue ( ( response.getStatus() == EXP_SUCCESS_STATUS) && (fId1 != fId2) && (pgnId1 != pgnId2) );
  	  }
//  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count ==10));
    }
    
    
    @Test
    @Parameters({"companyId","paginationlevel"})
    public void testFeedAPI_CompanyId_Pagination_MultiplePages(String companyId, String paginationlevel) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_Pagination_MultiplePages <<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId(client, companyId);
    	
  	  System.out.println("=====================================================================================");
  	  int count=0;
  	  String respEntity = response.getEntity(String.class);
  	  System.out.println(" ENTITY :"+respEntity );

  	
		 HashMap<String, Serializable> respMap1 = null;
		 ArrayList<HashMap<String, Serializable>> respMapLst1 = null;
		 
		 String pgnId1 = null;
		 String fId1 = null;
		  
		 String pgnId2 = null;
		 String fId2 = null;
		 
			 try {
				 respMap1 = new ObjectMapper().readValue(respEntity, HashMap.class);
				 System.out.println("respMap :"+respMap1.toString());
				 respMapLst1 = (ArrayList<HashMap<String, Serializable>>) respMap1.get("feeds");
				 System.out.println("respMapLst :"+respMapLst1.toString());
				 
			  		pgnId1 = (String) respMap1.get("pagination_id");
			  		fId1 = (String) respMapLst1.get(0).get("id");
			  		
			  		System.out.println("FIRST Id :"+fId1);
			  		System.out.println("FIRST pgn Id :"+pgnId1);

			 }
			 catch (JsonParseException e) {	e.printStackTrace(); } 
			 catch (JsonMappingException e) { e.printStackTrace(); } 
			 catch (IOException e) { e.printStackTrace(); }
			 
//			 System.out.println("0 :"+respMapLst.toString());
//			 System.out.println("1 :"+respMapLst.get(0));
//			 System.out.println("2 :"+respMapLst.get(0).containsKey("id"));
			 
			 for(int i=0; i<respMapLst1.size();i++){
				 if(respMapLst1.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }

//			 String resp_company_logo = (String) addSurveyMap.get(0).get("company_logo").toString();
//			 System.out.println("Response Data cLogo :"+resp_company_logo );
  	  
  	  System.out.println("Count :"+count);

  	  for(int i=1; i<=Integer.parseInt(paginationlevel); i++)
  	  {
  		  if(count == 10) {
  			  response = FeedAPI.invokeFeedAPI_CompanyId_PaginationId(client, companyId,"pgnId"+i);

  			  String pgnEntity = response.getEntity(String.class);
    	
  			  HashMap<String, Serializable> pgnMap = null;
  			  ArrayList<HashMap<String, Serializable>> pgnMapLst = null;
  		 
  			  try {
  				 pgnMap = new ObjectMapper().readValue(pgnEntity, HashMap.class);
  				 System.out.println("pgnMap :"+pgnMap.toString());
  				 pgnMapLst = (ArrayList<HashMap<String, Serializable>>) pgnMap.get("feeds");
  				 System.out.println("pgnMapLst :"+pgnMapLst.toString());
  				 
  			  		pgnId1 = (String) pgnMap.get("pagination_id");
  			  		fId2 = (String) pgnMapLst.get(0).get("id");
  			  		
  			  		System.out.println("SECOND Id :"+fId2);
  			  		System.out.println("SECOND pgnId :"+pgnId2);

  			  }
  			  catch (JsonParseException e) {	e.printStackTrace(); } 
  			  catch (JsonMappingException e) { e.printStackTrace(); } 
  			  catch (IOException e) { e.printStackTrace(); }
  		
  			for(int j=0; j<respMapLst1.size();j++){
				 if(respMapLst1.get(i).containsKey("id")  ){ 
					 count++;
					 }
				 else { System.out.println("Not Found");}
			 }
  			
  			assertTrue ( ( response.getStatus() == EXP_SUCCESS_STATUS) && (fId1 != fId2) && (pgnId1 != pgnId2) );
  		  }
  	  }
//  	  assertTrue(  (response.getStatus() == EXP_SUCCESS_STATUS) && (count ==10));
    }
    
   
    @Test
    @Parameters({"companyId"})
    public void testFeedAPI_CompanyId_Invalid(String companyId) throws IOException, Exception{
  	  	  
  	  System.out.println(">>>> testFeedAPI_CompanyId_InvalidCategory <<<<");
  	  ClientResponse response = FeedAPI.invokeFeedAPI_CompanyId_Invalid(client, companyId);
    	
  	  System.out.println("=====================================================================================");
  	  assertTrue(  (response.getStatus() == EXP_FAIL_STATUS_400) );
  	  
    }

    
*/    
    @AfterClass
    public void afterClass() {
    }
  }