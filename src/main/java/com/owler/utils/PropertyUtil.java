package com.owler.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertyUtil {
	public static String getProperty(String name, String FileFullPath) {
		String proppertyName = null;
		Properties properties = new Properties();
		try {
			// properties.load(PropertyUtil.class.getClassLoader()
			// .getResourceAsStream("conf/uiAutomationConf.properties"));
			properties.load(PropertyUtil.class.getClassLoader()
					.getResourceAsStream(FileFullPath));
			proppertyName = properties.getProperty(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return proppertyName;
	}
}
