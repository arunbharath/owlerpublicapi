/**
 * 
 */
package com.owler.api.feed;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.core.io.ClassPathResource;

import com.google.gson.JsonObject;
import com.owler.api.util.SignatureUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author Jay
 *
 * Modified the MethodSupported as 'GET' and parameters too.
 * 
 */
public class FeedAPI {

    public static ClientResponse invokeFeedAPI_CompanyId(Client client, String companyId) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/feed?company_id="+companyId);

				String endpointurl = props.getProperty("domain")+"/feed?company_id="+companyId;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
//                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeFeedAPI_CompanyId_limit(Client client, String companyId, String limit) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/feed?company_id="+companyId+"&limit="+limit);

				String endpointurl = props.getProperty("domain")+"/feed?company_id="+companyId+"&limit="+limit;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
//                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeFeedAPI_CompanyId_category(Client client, String companyId, String category) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/feed?company_id="+companyId+"&category="+category);

				String endpointurl = props.getProperty("domain")+"/feed?company_id="+companyId+"&category="+category;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
//                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    
    public static ClientResponse invokeFeedAPI_CompanyId_Invalid(Client client, String companyId) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/feed?company_id="+companyId);

				String endpointurl = props.getProperty("domain")+"/feed?company_id="+companyId;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
//                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeFeedAPI_CompanyId_category_NoResults(Client client, String companyId, String category) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/feed?company_id="+companyId+"&category="+category);

				String endpointurl = props.getProperty("domain")+"/feed?company_id="+companyId+"&category="+category;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
//                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeFeedAPI_CompanyId_limitLT10(Client client, String companyId, String limit) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/feed?company_id="+companyId+"&limit="+limit);

				String endpointurl = props.getProperty("domain")+"/feed?company_id="+companyId+"&limit="+limit;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
//                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeFeedAPI_CompanyId_limitGT100(Client client, String companyId, String limit) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/feed?company_id="+companyId+"&limit="+limit);

				String endpointurl = props.getProperty("domain")+"/feed?company_id="+companyId+"&limit="+limit;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
//                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    
    public static ClientResponse invokeFeedAPI_CompanyId_PaginationId(Client client, String companyId, String pgnId) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/feed?company_id="+companyId+"&pagination_id="+pgnId);

				String endpointurl = props.getProperty("domain")+"/feed?company_id="+companyId+"&pagination_id="+pgnId;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
//                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
}