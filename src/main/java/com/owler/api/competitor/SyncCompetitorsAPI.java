/**
 * Copyright (c) 2011 by InfoArmy Inc.  All Rights Reserved.
 * This file contains proprietary information of InfoArmy Inc.
 * Copying, use, reverse engineering, modification or reproduction of
 * this file without prior written approval is prohibited.
 *
 */
package com.owler.api.competitor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.core.MediaType;

import org.springframework.core.io.ClassPathResource;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author Jay
 * 
 */
public class SyncCompetitorsAPI {
	public static ClientResponse invokeSyncCompetitorIdAPI(Client client)
			throws Exception, IOException {
		System.out
				.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource(
				"conf/Domain.properties");
		Properties props = new Properties();

		try {
			props.load(propsResource.getInputStream());
		} catch (Exception e) {
			System.out.println("Exception in loading Domain.properties file");
		}

		WebResource service = client.resource(props.getProperty("domain")
				+ "/iris/oscar/syncCompetitors/");

		String endpointurl = props.getProperty("domain")
				+ "/iris/oscar/syncCompetitors/";
		System.out.println("End Point URL :" + endpointurl);

		HashMap<String, Object> requestMap = new HashMap<String, Object>();
		HashMap<Long, List<Long>> companyCompetitorMap = new HashMap();
		List<Long> competitorsList1 = new ArrayList();
		competitorsList1.add(100002L);
		competitorsList1.add(100006L);
		companyCompetitorMap.put(100001L, competitorsList1);
		List<Long> competitorsList2 = new ArrayList();
		competitorsList2.add(100009L);
		competitorsList2.add(100006L);
		companyCompetitorMap.put(100002L, competitorsList2);

		requestMap.put("memberId", 9876543);
		requestMap.put("companyCompetitorMap", companyCompetitorMap);

		ClientResponse response = null;
		try {
			response = service.entity(requestMap)
					.type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		System.out.println("Response Status :" + response.getStatus());
		System.out.println("Response Header :" + response.getHeaders());
		System.out.println("RESPONSE :" + response.getEntity(String.class));

		return response;
	}
}
