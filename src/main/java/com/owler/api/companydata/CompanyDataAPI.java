/**
 * 
 */
package com.owler.api.companydata;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.core.io.ClassPathResource;

import com.google.gson.JsonObject;
import com.owler.api.util.SignatureUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author Jay
 *
 * Modified the MethodSupported as 'GET' and parameters too.
 * 
 */
public class CompanyDataAPI {

    public static ClientResponse invokeCompanyDataAPI(Client client, String companyId) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/id/"+companyId);

				String endpointurl = props.getProperty("domain")+"/company/id/"+companyId;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeCompanyDataAPI_verifyXMLResponseStructure(Client client, String companyId) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/id/"+companyId+"?format=xml");

				String endpointurl = props.getProperty("domain")+"/company/id/"+companyId+"?format=xml";
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    
    public static ClientResponse invokeCompanyDataAPI_URL(Client client, String companyUrl) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/url/"+companyUrl);

				String endpointurl = props.getProperty("domain")+"/company/id/"+companyUrl;
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeCompanyDataAPI_URL_verifyXMLResponseStructure(Client client, String companyUrl) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/url/"+companyUrl+"?format=xml");

				String endpointurl = props.getProperty("domain")+"/company/url/"+companyUrl+"?format=xml";
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
}