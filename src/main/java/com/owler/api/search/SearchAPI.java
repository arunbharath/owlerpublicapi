/**
 * 
 */
package com.owler.api.search;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.core.io.ClassPathResource;

import com.google.gson.JsonObject;
import com.owler.api.util.SignatureUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author Jay
 *
 * Modified the MethodSupported as 'GET' and parameters too.
 * 
 */
public class SearchAPI {

    public static ClientResponse invokeSearchAPI_Name(Client client, String searchStr) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=name");

				String endpointurl = props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=name";
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeSearchAPI_InvalidFieldName(Client client, String searchStr) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=nameinvalid");

				String endpointurl = props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=nameinvalid";
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeSearchAPI_MaxLimitVal30(Client client, String searchStr) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=name&limit=30");

				String endpointurl = props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=name&limit=30";
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeSearchAPI_InvalidQueryStr(Client client, String searchStr) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/search?qinvalid="+searchStr+"&fields=name");

				String endpointurl = props.getProperty("domain")+"/company/search?qinvalid="+searchStr+"&fields=name";
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeSearchAPI_Website(Client client, String searchStr) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=website");

				String endpointurl = props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=website";
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
    public static ClientResponse invokeSearchAPI_Ticker(Client client, String searchStr) throws Exception, IOException {
    	System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		
				WebResource service = client.resource(props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=ticker");

				String endpointurl = props.getProperty("domain")+"/company/search?q="+searchStr+"&fields=ticker";
                System.out.println("End Point URL :"+endpointurl);
                
                ClientResponse response=null;
                try{
                	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
                }
                catch(Exception ex) {
                	ex.printStackTrace();
                }
                
                System.out.println("Response Status :"+response.getStatus());
                System.out.println("Response Header :"+response.getHeaders());
                System.out.println("RESPONSE :"+response.getEntity(String.class));
                
                return response;
        }
    
}