/*
 * Copyright (c) 2011 by InfoArmy Inc.  All Rights Reserved.
 * This file contains proprietary information of InfoArmy Inc.
 * Copying, use, reverse engineering, modification or reproduction of
 * this file without prior written approval is prohibited.
 *
 */
package com.owler.api.util;

import java.util.Map;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author kumar Jul 2, 2013
 * 
 */
public class SignatureUtil {
	public static String getSecretKey(String temp) {
		String[] temp1 = temp.split(":");
		String secret = temp1[1].substring(1, 11);
		return secret;
	}

	public static String getSignature(/* String emailId */String memberId,
			String uuid, String companyId, String secret) {
		String signature = "";
		Map<String, String> map = new TreeMap<String, String>();
		/* map.put("email", emailId); */
		map.put("memberId", memberId);
		map.put("uuid", uuid);
		if (companyId != null)
			map.put("companyId", companyId);
		String paramString = map.toString();
		int length = paramString.length();
		String encString = paramString.substring(1, length - 1);
		String ALG_HMAC = "HmacSHA1";
		try {
			SecretKeySpec signingKey = new SecretKeySpec(secret.getBytes(),
					ALG_HMAC);
			Mac mac = Mac.getInstance(ALG_HMAC);
			mac.init(signingKey);
			byte[] encBytes = mac.doFinal(encString.getBytes());
			/*
			 * String computedSignature = new
			 * String(org.apache.commons.codec.binary
			 * .Base64.encodeBase64(encBytes)).trim();
			 */
			signature = org.apache.commons.codec.binary.Base64
					.encodeBase64URLSafeString(encBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return signature;
	}

	public static String getSignature(String jsonString, String secret) {
		String signature = "";
		String ALG_HMAC = "HmacSHA1";
		try {
			SecretKeySpec signingKey = new SecretKeySpec(secret.getBytes(),
					ALG_HMAC);
			Mac mac = Mac.getInstance(ALG_HMAC);
			mac.init(signingKey);
			byte[] encBytes = mac.doFinal(jsonString.getBytes());
			/*
			 * String computedSignature = new
			 * String(org.apache.commons.codec.binary
			 * .Base64.encodeBase64(encBytes)).trim();
			 */
			signature = org.apache.commons.codec.binary.Base64
					.encodeBase64URLSafeString(encBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println(signature);
		return signature;
	}
}
