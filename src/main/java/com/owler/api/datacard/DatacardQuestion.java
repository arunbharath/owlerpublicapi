package com.owler.api.datacard;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.mongodb.DBObject;

public class DatacardQuestion {
	
		private String id;

		private long companyId;

		private String data_card_type;

		private HashMap<Double,Double> MeanSDValue; 
		
		private  double mean;
		
		private double SD;
		
		private int askFN = 2;
		
		private double lowerBoundValue;
		
		private double upperBoundValue;
		
		private double minRange;
		
		private double maxRange;
		
		public DatacardQuestion(DBObject document){
			this.id=(String)document.get("_id").toString();
			this.companyId=(Long) document.get("company_id");
			this.data_card_type=(String) document.get("data_card_type");
			this.MeanSDValue = (HashMap) document.get("data");
		}
		
		public String getQuestionId(){
			return id;
		}
		
		public long getCompanyId(){
			return companyId;
		}
		
		public String getDatacardType(){
			return data_card_type;
		}
		
		public double getMean(){
			return mean=MeanSDValue.get("mean");
		}
		
		public double getSD(){
			return SD=MeanSDValue.get("sd");
		}
		
		public Map<Double, Double> getData(){
			return MeanSDValue;
		}
		
		public double getLowerBoundValue(){
			mean=getMean();
			SD=getSD();
			double QuestionMean = lowerBoundValue=mean+(askFN*SD);
			return lowerBoundValue=QuestionMean - SD/5;
		}
		
		public double getUpperBoundValue(){
			mean=getMean();
			SD=getSD();
			double QuestionMean = upperBoundValue=mean+(askFN*SD);
			return upperBoundValue=QuestionMean + SD/5;
		}
}
