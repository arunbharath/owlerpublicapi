package com.owler.api.datacard;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.jersey.api.client.ClientResponse;

public class DatacardResponseVerification {
	
	double companyDiversityScore  = 0.5;
	double dcTypeDiversityScore = 0.5;
	static long dcSendexpiredays = 30;

	public static Boolean EMPLOYEE_StoreVoteVerrification(DatacardQuestion Response, ClientResponse response, String Vote) throws JSONException{
		String  ResponseToEventsPage = response.getEntity(String.class);
		JSONObject JsonResponse = new JSONObject(ResponseToEventsPage.trim());
    	if ((JsonResponse.getDouble("sd")==Response.getSD()) && (JsonResponse.getDouble("mean")==Response.getMean()) && (JsonResponse.getInt("companyId")==Response.getCompanyId()) && (JsonResponse.getString("questionType").equals(Response.getDatacardType())) && (JsonResponse.getString("vote_option").equals(Vote))){
    		return true;
    	}
    	else{
    		return false;
    	}
	}

	public static Boolean REVENUE_StoreVoteVerrification(DatacardQuestion Response, ClientResponse response, String Vote) throws JSONException{
		String  ResponseToEventsPage = response.getEntity(String.class);
		JSONObject JsonResponse = new JSONObject(ResponseToEventsPage.trim());
    	if ((JsonResponse.getDouble("sd")==Response.getSD()) && (JsonResponse.getDouble("mean")==Response.getMean()) && (JsonResponse.getInt("companyId")==Response.getCompanyId()) && (JsonResponse.getString("questionType").equals(Response.getDatacardType())) && (JsonResponse.getString("vote_option").equals(Vote))){
    		return true;
    	}
    	else{
    		return false;
    	}
	}
	
	public static Boolean COMPETITOR_StoreVoteVerrification(DatacardQuestion Response, ClientResponse response, String Vote) throws JSONException{
		String  ResponseToEventsPage = response.getEntity(String.class);
		JSONObject JsonResponse = new JSONObject(ResponseToEventsPage.trim());
    	if ((JsonResponse.getInt("companyId")==Response.getCompanyId()) && (JsonResponse.getString("questionType").equals(Response.getDatacardType())) && (JsonResponse.getString("vote_option").equals(Vote))){
    		return true;
    	}
    	else{
    		return false;
    	}
	}
	
	public static Boolean SECTOR_StoreVoteVerrification(DatacardQuestion Response, ClientResponse response, String Vote) throws JSONException{
		String  ResponseToEventsPage = response.getEntity(String.class);
		JSONObject JsonResponse = new JSONObject(ResponseToEventsPage.trim());
    	if ((JsonResponse.getInt("companyId")==Response.getCompanyId()) && (JsonResponse.getString("questionType").equals(Response.getDatacardType())) && (JsonResponse.getString("vote_option").equals(Vote))){
    		return true;
    	}
    	else{
    		return false;
    	}
	}
	
	public static Boolean SD_StoreVoteVerrification(DatacardQuestion Response, ClientResponse response, String Vote) throws JSONException{
		String  ResponseToEventsPage = response.getEntity(String.class);
		JSONObject JsonResponse = new JSONObject(ResponseToEventsPage.trim());
    	if ((JsonResponse.getInt("companyId")==Response.getCompanyId()) && (JsonResponse.getString("questionType").equals(Response.getDatacardType())) && (JsonResponse.getString("vote_option").equals(Vote))){
    		return true;
    	}
    	else{
    		return false;
    	}
	}

	public static Boolean LO_StoreVoteVerrification(DatacardQuestion Response, ClientResponse response, String Vote) throws JSONException{
		String  ResponseToEventsPage = response.getEntity(String.class);
		JSONObject JsonResponse = new JSONObject(ResponseToEventsPage.trim());
    	if ((JsonResponse.getInt("companyId")==Response.getCompanyId()) && (JsonResponse.getString("questionType").equals(Response.getDatacardType())) && (JsonResponse.getString("vote_option").equals(Vote))){
    		return true;
    	}
    	else{
    		return false;
    	}
	}
	
	public static Boolean CA_StoreVoteVerrification(DatacardQuestion Response, ClientResponse response, String Vote) throws JSONException{
		String  ResponseToEventsPage = response.getEntity(String.class);
		JSONObject JsonResponse = new JSONObject(ResponseToEventsPage.trim());
    	if ((JsonResponse.getInt("companyId")==Response.getCompanyId()) && (JsonResponse.getString("questionType").equals(Response.getDatacardType())) && (JsonResponse.getString("vote_option").equals(Vote))){
    		return true;
    	}
    	else{
    		return false;
    	}
	}
	
	public static Boolean Get_DCEndPointToEventsPage(ClientResponse response) throws JSONException,IOException{
		String  responseToEventsPage = response.getEntity(String.class);
		JSONArray jsonArray = new JSONArray(responseToEventsPage.trim());
    	if (jsonArray.isNull(0)){
    		return false;
    	}
    	else{
    		return true;
    	}
	}
	
	public static Boolean Get_DCToSnapshot(ClientResponse response, DatacardHistory dcHistoryResponse) throws JSONException,IOException{
		
		long expiretime = dcHistoryResponse.getExpireDateEpoch();
		long updatetime = dcHistoryResponse.getUpdateDateEpoch();
		long days = TimeUnit.MILLISECONDS.toDays(expiretime-updatetime);
    	if (days==dcSendexpiredays){
    		return true;
    	}
    	else{
    		return false;
    	}
    	
	}

}
