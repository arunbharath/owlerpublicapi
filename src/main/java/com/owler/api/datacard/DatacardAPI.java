package com.owler.api.datacard;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MediaType;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.core.io.ClassPathResource;

import com.google.gson.JsonObject;
import com.owler.api.util.SignatureUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


public class DatacardAPI {
	
	public static ClientResponse REVENUE_DCEndPointToEventsPageStoreVote(Client client, DatacardQuestion Response, String EmployeeVote){
		System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();

		long MemberId = 1012363;
		ClientResponse response=null;
				
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		if(EmployeeVote.contains("DONT_KNOW")){
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\",\"mean\":"+Response.getMean()+",\"sd\":"+Response.getSD()+"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            } 
		}
		if((EmployeeVote.contains("_LB")) || (EmployeeVote.contains("_LT_"))){
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\",\"mean\":"+Response.getMean()+",\"sd\":"+Response.getSD()+",\"lowerBoundRevenue\":"+Response.getLowerBoundValue()+"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            }
		}

		if((EmployeeVote.contains("_UB")) || (EmployeeVote.contains("_GT_"))){
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\",\"mean\":"+Response.getMean()+",\"sd\":"+Response.getSD()+",\"upperBoundRevenue\":"+Response.getUpperBoundValue()+"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            }
		}

		if((EmployeeVote.contains("REV_IN_RANGE"))){
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\",\"mean\":"+Response.getMean()+",\"sd\":"+Response.getSD()+",\"minRange\":"+Response.getLowerBoundValue()+",\"maxRange\":"+Response.getUpperBoundValue()+"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            }
		}
		return response;
	}

	public static ClientResponse EMPLOYEE_DCEndPointToEventsPageStoreVote(Client client, DatacardQuestion Response, String EmployeeVote){
		System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();

		long MemberId = 1012363;
		ClientResponse response=null;
				
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}
		if(EmployeeVote.contains("DONT_KNOW")){
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\",\"mean\":"+Response.getMean()+",\"sd\":"+Response.getSD()+"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            } 
		}
		if((EmployeeVote.contains("_LB")) || (EmployeeVote.contains("_LT_"))){
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\",\"mean\":"+Response.getMean()+",\"sd\":"+Response.getSD()+",\"lowerBoundEmployee\":"+Response.getLowerBoundValue()+"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            }
		}

		if((EmployeeVote.contains("_UB")) || (EmployeeVote.contains("_GT_"))){
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\",\"mean\":"+Response.getMean()+",\"sd\":"+Response.getSD()+",\"upperBoundEmployee\":"+Response.getUpperBoundValue()+"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            }
		}

		if((EmployeeVote.contains("EMP_IN_RANGE"))){
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\",\"mean\":"+Response.getMean()+",\"sd\":"+Response.getSD()+",\"minRange\":"+Response.getLowerBoundValue()+",\"maxRange\":"+Response.getUpperBoundValue()+"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            }
		}
		return response;
	}	
	
	public static ClientResponse COMPETITOR_DCEndPointToEventsPageStoreVote(Client client, DatacardQuestion Response, String EmployeeVote){
		System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();

		long MemberId = 1012363;
		ClientResponse response=null;
				
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}		
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            } 
		return response;
	}

	public static ClientResponse SECTOR_DCEndPointToEventsPageStoreVote(Client client, DatacardQuestion Response, String EmployeeVote){
		System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();

		long MemberId = 1012363;
		ClientResponse response=null;
				
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}		
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            } 
		return response;
	}

	public static ClientResponse SD_DCEndPointToEventsPageStoreVote(Client client, DatacardQuestion Response, String EmployeeVote){
		System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();

		long MemberId = 1012363;
		ClientResponse response=null;
				
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}		
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            } 
		return response;
	}

	public static ClientResponse LO_DCEndPointToEventsPageStoreVote(Client client, DatacardQuestion Response, String EmployeeVote){
		System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();

		long MemberId = 1012363;
		ClientResponse response=null;
				
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}		
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            } 
		return response;
	}

	public static ClientResponse CA_DCEndPointToEventsPageStoreVote(Client client, DatacardQuestion Response, String EmployeeVote){
		System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();

		long MemberId = 1012363;
		ClientResponse response=null;
				
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
			}		
			WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/storeVote");
			String Body = "{\"memberId\":\""+MemberId+"\",\"parameter_map\":{\"vote_option\":\""+EmployeeVote+"\",\"dataCardId\":\""+Response.getQuestionId()
				+"\",\"dataCardSource\":\"web\"}}";
			String endpointurl = props.getProperty("Datacard_domain")+"/datacards/storeVote";
            System.out.println("End Point URL :"+endpointurl);
                
            try{
            	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,Body);
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            } 
		return response;
	}
	
	public static ClientResponse Get_DCEndPointToEventsPage(Client client, long memberId, long companyId){
		System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		ClientResponse response=null;
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
		}
		WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/get");
		String body = "{\"memberId\":"+memberId+",\"companyIds\":["+companyId+"],\"ignoreRelevancyMap\":true,\"src\":\"Automation\"}";
		String endpointurl = props.getProperty("Datacard_domain")+"/datacards/get";
        System.out.println("End Point URL :"+endpointurl);
            
        try{
        	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,body);
        }
        catch(Exception ex) {
        	ex.printStackTrace();
        } 
	return response;
	}
	
	public static ClientResponse Get_DCToSnapshot(Client client, long memberId, long companyId){
		System.out.println("===================================================================");
		ClassPathResource propsResource = new ClassPathResource("conf/Domain.properties");
		Properties props = new Properties();
		
		ClientResponse response=null;
		
		try {
			props.load(propsResource.getInputStream());
		}
		catch(Exception e){ 
			System.out.println("Exception in loading Domain.properties file");
		}
		WebResource service = client.resource(props.getProperty("Datacard_domain")+"/datacards/"+memberId);
		String body = "{\"memberId\":"+memberId+",\"companyIds\":["+companyId+"],\"numOfQuestions\":1,\"src\":\"Automation\"}";
		String endpointurl = props.getProperty("Datacard_domain")+"/datacards/get";
        System.out.println("End Point URL :"+endpointurl);
            
        try{
        	response = service.header("endpointurl", endpointurl).type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class,body);
        }
        catch(Exception ex) {
        	ex.printStackTrace();
        } 
	return response;
	
	}
}
