package com.owler.library;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.NavigableMap;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;
import java.net.UnknownHostException;

import static java.lang.Integer.parseInt;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;
import com.mongodb.util.JSON;
import com.mongodb.util.JSONParseException;

import static com.mongodb.util.JSON.parse;

import com.owler.api.datacard.DatacardHistory;
import com.owler.api.datacard.DatacardQuestion;
import com.owler.utils.PropertyUtil;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.IFeatureAwareVersion;
import de.flapdoodle.embed.process.distribution.GenericVersion;


public class MongoDB {
	
	private MongoClient mongoClient;
	private static DB db;
	
	public void ConnectToMongoDatabase(String databaseName) {
		String server = null, port = null, database = null, username = null, password = null;
		String serverPort;
		try {
			if (databaseName != null) {
				if (databaseName.trim().equals("owler_audit")) {
					serverPort = PropertyUtil.getProperty("mongoServerUrlsQe",
							"conf/Mongo.properties");
					database = PropertyUtil.getProperty("owler_audit",
							"conf/Mongo.properties");
					server = serverPort.split(":")[0];
					port = serverPort.split(":")[1];
					username = PropertyUtil.getProperty("user",
							"conf/Mongo.properties");
					password = PropertyUtil.getProperty("password",
							"conf/Mongo.properties");
				} else if (databaseName.trim().equals("owler_audit")) {
					serverPort = PropertyUtil.getProperty(
							"mongoServerUrlsAdhoc", "conf/Mongo.properties");
					database = PropertyUtil.getProperty("owler_audit",
							"conf/Mongo.properties");
					server = serverPort.split(":")[0];
					port = serverPort.split(":")[1];
					username = PropertyUtil.getProperty("userAdhoc",
							"conf/Mongo.properties");
					password = PropertyUtil.getProperty("passwordAdhoc",
							"conf/Mongo.properties");

				} else if (databaseName.trim().equals("owler_audit")) {
					serverPort = PropertyUtil.getProperty(
							"mongoServerUrlsRelease", "conf/Mongo.properties");
					database = PropertyUtil.getProperty("owler_audit",
							"conf/Mongo.properties");
					server = serverPort.split(":")[0];
					port = serverPort.split(":")[1];
					username = PropertyUtil.getProperty("userRelease",
							"conf/Mongo.properties");
					password = PropertyUtil.getProperty("passwordRelease",
							"conf/Mongo.properties");
				}
				if (databaseName.trim().equals("owler_audit")) {
					serverPort = PropertyUtil.getProperty(
							"mongoServerUrlsAutomation",
							"conf/Mongo.properties");
					database = PropertyUtil.getProperty("owler_audit",
							"conf/Mongo.properties");
					server = serverPort.split(":")[0];
					port = serverPort.split(":")[1];
					username = PropertyUtil.getProperty("userAutomation",
							"conf/Mongo.properties");
					password = PropertyUtil.getProperty("passwordAutomation",
							"conf/Mongo.properties");
				}

				else if (databaseName.trim().equals("iris")) {
					serverPort = PropertyUtil.getProperty("mongoServerUrlsQe",
							"conf/Mongo.properties");
					database = PropertyUtil.getProperty("irisDatabase",
							"conf/Mongo.properties");
					server = serverPort.split(":")[0];
					port = serverPort.split(":")[1];
					username = PropertyUtil.getProperty("user",
							"conf/Mongo.properties");
					password = PropertyUtil.getProperty("password",
							"conf/Mongo.properties");

				} else if (databaseName.trim().equals("iris")) {
					serverPort = PropertyUtil.getProperty(
							"mongoServerUrlsAdhoc", "conf/Mongo.properties");
					database = PropertyUtil.getProperty("irisDatabase",
							"conf/Mongo.properties");
					server = serverPort.split(":")[0];
					port = serverPort.split(":")[1];
					username = PropertyUtil.getProperty("user",
							"conf/Mongo.properties");
					password = PropertyUtil.getProperty("password",
							"conf/Mongo.properties");
				} else if (databaseName.trim().equals("iris")) {
					serverPort = PropertyUtil.getProperty(
							"mongoServerUrlsRelease", "conf/Mongo.properties");
					database = PropertyUtil.getProperty("irisDatabase",
							"conf/Mongo.properties");
					server = serverPort.split(":")[0];
					port = serverPort.split(":")[1];
					username = PropertyUtil.getProperty("user",
							"conf/Mongo.properties");
					password = PropertyUtil.getProperty("password",
							"conf/Mongo.properties");
				}

				else if (databaseName.trim().equals("iris")) {
					serverPort = PropertyUtil.getProperty(
							"mongoServerUrlsPerformance",
							"conf/Mongo.properties");
					database = PropertyUtil.getProperty("irisDatabase",
							"conf/Mongo.properties");
					server = serverPort.split(":")[0];
					port = serverPort.split(":")[1];
					username = PropertyUtil.getProperty("user",
							"conf/Mongo.properties");
					password = PropertyUtil.getProperty("password",
							"conf/Mongo.properties");
				} else if (databaseName.trim().equals("spider_test")) {
					serverPort = PropertyUtil.getProperty("mongoServerUrlsQe",
							"conf/Mongo.properties").trim();
					database = PropertyUtil.getProperty("spiderDatabase",
							"conf/Mongo.properties").trim();
					server = serverPort.split(":")[0].trim();
					port = serverPort.split(":")[1].trim();
					username = PropertyUtil.getProperty("user",
							"conf/Mongo.properties").trim();
					password = PropertyUtil.getProperty("password",
							"conf/Mongo.properties").trim();
				} else if (databaseName.trim().equals("owler_email")) {
					serverPort = PropertyUtil.getProperty("mongoServerUrlsQe",
							"conf/Mongo.properties").trim();
					database = PropertyUtil.getProperty("emailDatabase",
							"conf/Mongo.properties").trim();
					server = serverPort.split(":")[0].trim();
					port = serverPort.split(":")[1].trim();
					username = PropertyUtil.getProperty("user",
							"conf/Mongo.properties").trim();
					password = PropertyUtil.getProperty("password",
							"conf/Mongo.properties").trim();
				} else if (databaseName.trim().equals("owler_contribution")) {
					serverPort = PropertyUtil.getProperty("mongoServerUrlsQe",
							"conf/Mongo.properties").trim();
					database = PropertyUtil.getProperty("datacardDatabase",
							"conf/Mongo.properties").trim();
					server = serverPort.split(":")[0].trim();
					port = serverPort.split(":")[1].trim();
					username = PropertyUtil.getProperty("user",
							"conf/Mongo.properties").trim();
					password = PropertyUtil.getProperty("password",
							"conf/Mongo.properties").trim();
				}
				
				else if (databaseName.trim().equals("release.owler.com")) {
					serverPort = PropertyUtil.getProperty(
							"mongoServerUrlsRelease", "conf/Mongo.properties");
					database = PropertyUtil.getProperty("databaseRelease",
							"conf/Mongo.properties");
					server = serverPort.split(":")[0];
					port = serverPort.split(":")[1];
					username = PropertyUtil.getProperty("userRelease",
							"conf/Mongo.properties");
					password = PropertyUtil.getProperty("passwordRelease",
							"conf/Mongo.properties");
				}
				else {
					throw new RuntimeException(
							"Provided databaseName should be either iris or audit db ");
				}

			}
			mongoClient = new MongoClient(server, parseInt(port));
			db = mongoClient.getDB(database);
			db.authenticate(username, password.toCharArray());
	}
		catch (UnknownHostException e) {
			throw new MongodbLibraryException("error connecting mongodb", e);	
		}

	}
	
	public List<String> getEmailContentWithLimit(String collectionName,
			String jsonString, String  projectField,String Key,int limit) {
		List<String> ret1 = new ArrayList<String>();
		List<String> ret2 = new ArrayList<String>();
		for (DBObject document : db.getCollection(collectionName).find(
				(DBObject) parse(jsonString),(DBObject) parse(projectField)).limit(limit)) {
			ret1.add((String) document.get(Key));
		}
		for(String temp : ret1){
			temp = temp.replace("s3.amazonaws.com/email-content-test", "email-content-test.s3.amazonaws.com");
			ret2.add(temp);
		}
		return ret2;
	}

	public static HashMap<String,List<Long>> getDatacardQuestionId(List<Long> CompanyIds){
		
		String QuestionIds;
		String Query1 = "{company_id:{$in:"+CompanyIds+"}}";
		String Query2 = "{_id:1,company_id:1,demand_score:1,data_card_type:1}";
		HashMap<String,List<Long>> DemandPoolDetails= new HashMap<String,List<Long>>();
		for (DBObject document : db.getCollection("demand_pool").find(
				(DBObject) parse(Query1),(DBObject) parse(Query2))){
			QuestionIds = document.get("_id").toString();
			long CompanyId1 = (Long) document.get("company_id"); 
			long DemandScore = (Long) document.get("demand_score");
			List<Long> companyIddemandScore  = new ArrayList<Long>();
			companyIddemandScore.add(CompanyId1);
			companyIddemandScore.add(DemandScore);
			DemandPoolDetails.put(QuestionIds,companyIddemandScore);
		}
		return DemandPoolDetails;
	}

	public static HashMap<String,String> getDatacardType(List<Long> CompanyIds){
		
		String QuestionIds;
		String Query1 = "{company_id:{$in:"+CompanyIds+"}}";
		String Query2 = "{_id:1,company_id:1,demand_score:1,data_card_type:1}";
		String DatacardsType;
		HashMap<String,String> DemandPoolDetails= new HashMap<String,String>();
		for (DBObject document : db.getCollection("demand_pool").find(
				(DBObject) parse(Query1),(DBObject) parse(Query2))){
			QuestionIds = document.get("_id").toString();
			DatacardsType = document.get("data_card_type").toString();
			DemandPoolDetails.put(QuestionIds,DatacardsType);
		}
		return DemandPoolDetails;
	}
	
	public static List<Long> getDCDiversityCompany(long MemberId){
		
		String Query1 = "{member_id:"+MemberId+"}";
		String Query2 = "{member_id:1,company_id:1,update_ts:1}";
		String Query3 = "{update_ts:-1}";
		HashMap<Long,Long> hashMap = new HashMap<Long,Long>();
		TreeMap<Long,Long> treeMap = new TreeMap<Long,Long>();
		for (DBObject document : db.getCollection("data_cards_history").find(
				(DBObject) parse(Query1),(DBObject) parse(Query2)).sort((DBObject) parse(Query3)).limit(3)){
			long companyId = (Long) document.get("company_id");
			Date data = new Date();
			data = (Date) document.get("update_ts");
			long date = data.getTime();
			if(hashMap.containsKey(companyId)==true){
				long temp = hashMap.get(companyId);
				if (temp<date){
					hashMap.put(companyId,date);
					treeMap.put(date,companyId);
				}
			}
			else{
				hashMap.put(companyId,date);
				treeMap.put(date,companyId);
			}
		}
		for (DBObject document : db.getCollection("data_cards_response").find(
				(DBObject) parse(Query1),(DBObject) parse(Query2)).limit(3).sort((DBObject) parse(Query3))){
			long companyId = (Long) document.get("company_id");
			Date data = new Date();
			data = (Date) document.get("update_ts");
			long date = data.getTime();
			if(hashMap.containsKey(companyId)==true){
				long temp = hashMap.get(companyId);
				if (temp<date){
					hashMap.put(companyId,date);
					treeMap.put(date,companyId);
				}
			}
			else{
				hashMap.put(companyId,date);
				treeMap.put(date,companyId);
			}
			//List<Long> DCCompanyIds = DSCompanyCollection.ke
		}
		NavigableMap DSCompanySorted = treeMap.descendingMap();
		List<Long> DSCompanyId = new ArrayList<Long>(DSCompanySorted.values());
		int size = DSCompanyId.size();
		if (size>3){
			DSCompanyId.subList(3, size).clear();
		}
		return DSCompanyId;
	}

	public static List<String> getDCDiversityCategory(long MemberId){
		
		String Query1 = "{member_id:"+MemberId+"}";
		String Query2 = "{member_id:1,question_type:1,update_ts:1}";
		String Query3 = "{update_ts:-1}";
		HashMap<String,Long> hashMap = new HashMap<String,Long>();
		TreeMap<Long,String> treeMap = new TreeMap<Long,String>();
		for (DBObject document : db.getCollection("data_cards_history").find(
				(DBObject) parse(Query1),(DBObject) parse(Query2)).sort((DBObject) parse(Query3)).limit(3)){
			String DCcategory = (String) document.get("question_type");
			Date data = new Date();
			data = (Date) document.get("update_ts");
			long date = data.getTime();
			if(hashMap.containsKey(DCcategory)==true){
				long temp = hashMap.get(DCcategory);
				if (temp<date){
					hashMap.put(DCcategory, date);
					treeMap.put(date, DCcategory);
				}
			}
			else{
				hashMap.put(DCcategory, date);
				treeMap.put(date, DCcategory);
			}
		}
		Query2 = "{member_id:1,data_card_type:1,update_ts:1}";
		for (DBObject document : db.getCollection("data_cards_response").find(
				(DBObject) parse(Query1),(DBObject) parse(Query2)).limit(3).sort((DBObject) parse(Query3))){
			String DCcategory = (String) document.get("data_card_type");
			Date data = new Date();
			data = (Date) document.get("update_ts");
			long date = data.getTime();
			if(hashMap.containsKey(DCcategory)==true){
				long temp = hashMap.get(DCcategory);
				if (temp<date){
					hashMap.put(DCcategory, date);
					treeMap.put(date, DCcategory);
				}
			}
			else{
				hashMap.put(DCcategory, date);
				treeMap.put(date, DCcategory);
			}
		}
		NavigableMap DSCategorySorted = treeMap.descendingMap();
		List<String> DSCategory = new ArrayList<String>(DSCategorySorted.values());
		int size = DSCategory.size();
		if(size>3){
			DSCategory.subList(3, size).clear();
		}
		return DSCategory;
	}
	
	public static HashMap<String,Boolean> getQuestionAppropriate(long MemberId,Set<String> Questions){
		
		String Query1 = "{member_id:"+MemberId+"}";
		String Query2 = "{demand_pool_id:1}";
		HashMap<String,Boolean> AppropriateScore = new HashMap<String,Boolean>();
		for (DBObject document : db.getCollection("data_cards_history").find(
				(DBObject) parse(Query1),(DBObject) parse(Query2))){
			String QuestionId = (String) document.get("demand_pool_id");
			if (Questions.contains(QuestionId)){
				AppropriateScore.put(QuestionId, false);
			}		
		}
	return AppropriateScore;
	}
	
	public static DatacardQuestion getDatacardQuestion(String datacardType,int limit) throws JSONParseException{

		DatacardQuestion response = null;
		String Query ="{data_card_type:\""+datacardType+"\"}";
		for (DBObject document : db.getCollection("demand_pool").find((DBObject) parse(Query)).limit(1)){
		response = new DatacardQuestion(document);
		}
		return response;
	}
	
	public static DatacardHistory getDCHistory(String questionID, long memberId){
		
		DatacardHistory response = null;
		String query = "{demand_pool_id:\""+questionID+"\",member_id:"+memberId+"}";
		for(DBObject document : db.getCollection("data_cards_history").find((DBObject) parse(query))){
			response = new DatacardHistory(document);
		}
		return response;
	}

}

